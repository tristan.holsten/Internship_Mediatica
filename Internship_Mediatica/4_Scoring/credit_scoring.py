import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import copy
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import ParameterGrid
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import validation_curve
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import VotingClassifier
from sklearn import svm
from sklearn.metrics import roc_auc_score
from sklearn.dummy import DummyClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import f1_score
from sklearn.utils.class_weight import compute_class_weight
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier


###################################### Some Functions #################################################

#to do the grid search for the enemble classifier
def train_algorithms(model, param_grid, name):
    algorithm_list=[]
    param_grid_list=list(param_grid)
    for k, params in enumerate(param_grid_list):
        clf=copy.deepcopy(model.set_params(**params))
        algorithm_list.append((name+str(k),clf))
    return algorithm_list

#the ANN structure
def create_model(optimizer='adam'):
    model = Sequential()
    model.add(Dense(100, activation='relu', input_dim=k_features))
    model.add(Dropout(0.2))
    model.add(Dense(20, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'], weighted_metrics=['accuracy'])
    return model

#supress warnings
pd.set_option('mode.chained_assignment', None)

####################### Data preprocessing #######################################
np.random.seed(42)

#load the data file (in this case Conti)
df = pd.read_csv('../2_Preprocessing/Data/Output/Datatape_Emilbanca_taxonomy_Conti_preprocessed.csv', header=[0,1,2])

#print(df.shape)
#extract target values
target_raw=df['% credito recuperato']
data=df.drop(columns=['Andamento recupero', '% credito recuperato'], level=0)

#number of features to work with
#len(data.columns)

#convert target array to binary targets
target_array=np.ones(len(target_raw.values))
#every credit was assumed to be closed and 1 was assigned to the credits which are over 20%recovered
target_array[target_raw.values[:,0]>0]=0
target=pd.Series(target_array, name=('% credito recuperato', '% credito recuperato','% credito recuperato'))

############################# Feature Selection section ####################################

unique_variables=[]
#print(sum(target)/len(target), (len(target)-sum(target))/len(target))
for (columnName, columnData) in data.iteritems():
    if len(np.unique(columnData.values))==1:
        unique_variables.append(columnName)
#print(df.isnull().values.any())
#print(unique_variables, len(unique_variables))
#print(data.shape)
data.drop(columns=unique_variables, inplace=True)

feature_corr= data.corr()
uncorr_features = np.full((feature_corr.shape[0],), True, dtype=bool)
for i in range(feature_corr.shape[0]):
   for j in range(i+1, feature_corr.shape[0]):
        if np.abs(feature_corr.iloc[i,j]) >= 0.7:
            if uncorr_features[j]:
                uncorr_features[j] = False
data=data[data.columns[uncorr_features]]

############################# Feature Importance #############################################
feature_corr_target= pd.concat([data, target], axis=1).corr()
corr_target=abs(feature_corr_target[('% credito recuperato', '% credito recuperato','% credito recuperato')])
feature_score_corr=pd.Series(corr_target, index=df.columns)
feature_score_corr.drop(('% credito recuperato', '% credito recuperato','% credito recuperato'), inplace=True)
feature_score_corr.index=feature_score_corr.index.droplevel([0,1])
feature_score_corr_sum=feature_score_corr.sum()
feature_score_corr_relative=feature_score_corr/feature_score_corr_sum
k_features=len(data.columns)
print('k-features', k_features)
#print("Most important features suggested by Correations\n",feature_score_corr_relative.nlargest(k_features), '\n')
plt.figure(figsize=(8,5))
(feature_score_corr_relative*100).nlargest(30).plot(kind='barh')
plt.title('Feature importance Correlations')
plt.xlabel('document importance in %')
plt.tight_layout()
plt.savefig('Pictures/feature_importance_credit_scoring_correlations.pdf')
plt.close()


#feature selection by  F-Test method (quiet the same as correlations)
best_features=SelectKBest(score_func=f_classif, k=k_features)
fit=best_features.fit(data, target)
feature_score_F = pd.Series(fit.scores_, index=data.columns)
feature_score_F.index=feature_score_F.index.droplevel([0,1])
F1_score_sum=feature_score_F.sum()
F1_Feature_score_relative=feature_score_F/F1_score_sum
#print('Most important features suggested by F_Test \n',F1_Feature_score_relative.nlargest(k_features),'\n')
(F1_Feature_score_relative*100).nlargest(30).plot(kind='barh')
plt.title('Feature importance F_score')
plt.xlabel('document importance in %')
plt.tight_layout()
plt.savefig('Pictures/feature_importance_credit_scoring_F1.pdf')
plt.close()


#feature importances by decision tree
model_trees=ExtraTreesClassifier(n_estimators=50)
model_trees.fit(data,target)
feature_importances=pd.Series(model_trees.feature_importances_, index=data.columns)
feature_importances_classes=feature_importances.sum(level=0).sort_values(ascending=True)
feature_importances_sub=feature_importances.sum(level=1).sort_values(ascending=True)
#print(feature_importances_classes)
feature_importances_original=copy.deepcopy(feature_importances)

feature_importances.index=feature_importances.index.droplevel([0,1])
#print("Most important features suggested by Random Forrest\n",feature_importances.nlargest(k_features), "\n")

#plot with the use of Xtratreesclassifier the most important documents for the decision (also document (sub-)classes
(feature_importances*100).nlargest(30).sort_values(ascending=True).plot(kind='barh')
plt.title('Feature importance RF')
plt.xlabel('document importance in %')
plt.tight_layout()
plt.savefig('Pictures/feature_importance_credit_scoring_XtraRF.pdf')
plt.close()

(feature_importances_classes*100).plot(kind='barh')
plt.title('Feature importance RF')
plt.xlabel('Class importance in %')
plt.tight_layout()
plt.savefig('Pictures/feature_importance_classes_credit_scoring_XtraRF.pdf')
plt.close()

(feature_importances_sub*100).plot(kind='barh')
plt.title('Feature importance RF')
plt.xlabel('Subclass importance in %')
plt.tight_layout()
plt.savefig('Pictures/feature_importance_sub_credit_scoring_XtraRF.pdf')
plt.close()

important_features=feature_importances_original.nlargest(k_features).index

############### Algorithm Section ############

X_train, X_test, y_train, y_test = train_test_split(data, target, test_size=0.2, stratify=target)
X_train=X_train[important_features]
X_test=X_test[important_features]
#repeated stratified k fold used beacuase of small dataset, f it is bigger maybe normal stratified kfold is enough (right now computational expensive
kfold = RepeatedStratifiedKFold(n_splits=5, n_repeats=5)
#x=np.arange(2)
#for set in [target, y_test, y_train]:
#    plt.bar(x, [len(set)-set.sum(), set.sum()])
#    plt.show()
#exit()


######################## Dummy Classifier ###########################################

dummy=DummyClassifier()
dummy.fit(X_train, y_train)
balanced_score=balanced_accuracy_score(y_test, dummy.predict(X_test))
print("Balanced accuracy on the test set by Dummy:", balanced_score)


######################## Ensemble Algorithm (SVC, Logistic Regression, Decision Trees) ###########################
probability_predictions=[]

grid_param_RF ={'n_estimators': [5,10,20,100], 'max_depth': [None,5],
               'max_leaf_nodes': [55,65], 'min_samples_leaf': [2,5,10]}
grid_param_SVC ={'kernel': ['rbf'], 'gamma': [0.25,0.3,1e-1, 0.4e-1,0.35],
                    'C': [0.1,0.9,1,5,10], 'class_weight':['balanced']}
grid_param_LR={'penalty': ['l1','l2'], 'C': [0.1, 1.0, 10, 100,50, 1000], 'class_weight':['balanced'], 'solver':['liblinear']}

Grid_SVC=ParameterGrid({'kernel': ['rbf'], 'gamma': [0.3,1e-1,0.5e-1, 0.7e-1, 1e-2],
                    'C': [0.1,1,10,40,50,60,100], 'class_weight':['balanced'], 'probability':[True]})
Grid_LR=ParameterGrid({'penalty': ['l1','l2'], 'C': [0.1,0.4,0.5,0.7,1.0], 'class_weight':['balanced'], 'solver':['liblinear']})
Grid_Tree=ParameterGrid({'max_depth':[None,3, 5, 10,100], 'min_samples_split':[2,4,6,8,20], 'min_samples_leaf':[1,2,4,6,8], 'max_leaf_nodes':[None,3,5,10]})


SVC_classifier=train_algorithms(svm.SVC(), Grid_SVC, 'SVC_')
print("grid_search SVC complete")
LR_classifier=train_algorithms(LogisticRegression(), Grid_LR, 'LR_')
print("grid_search LR complete")
Tree_classifier=train_algorithms(DecisionTreeClassifier(), Grid_Tree, 'Tree_')
print("grid_search Tree complete")

all_classifiers=SVC_classifier+LR_classifier+Tree_classifier
algorithm_set=[]
ensemble_score=0.0


# check program efficiency
while all_classifiers:
    best_add_alg = ()
    for algorithm in all_classifiers:
        #print([*algorithm_set,algorithm])
        eclf=VotingClassifier(estimators=[*algorithm_set,algorithm], voting='soft')
        new_ensemble_score=np.mean(cross_val_score(eclf, X_train, y_train, cv=kfold, scoring='balanced_accuracy'))
        if new_ensemble_score > ensemble_score:
            best_add_alg=algorithm
            print(new_ensemble_score)
            ensemble_score=new_ensemble_score
    algorithm_set.append(best_add_alg)
    #print(algorithm_set)
    if not best_add_alg:
        del algorithm_set[-1]
        break
    all_classifiers.remove(best_add_alg)

print('The choosen Ensemble classifier consists out of:', algorithm_set)
best_eclf=VotingClassifier(estimators=algorithm_set, voting='soft')
best_eclf.fit(X_train, y_train)
#print(best_eclf.classes_)
#print(best_eclf.predict_proba(X_test))
print('The Ensemble probability for the {} test samples that money will be recovered is:'.format(len(y_test)), np.squeeze(best_eclf.predict_proba(X_test)[:,np.where(best_eclf.classes_==1)])*100)
probability_predictions.append(['E_Hill',np.squeeze(best_eclf.predict_proba(X_test)[:,np.where(best_eclf.classes_==1)])*100])
ensemble_score=balanced_accuracy_score(y_test,best_eclf.predict(X_test))
print("Balanced accuracy on the test set by Ensemble Hillclimbing", ensemble_score)


################################ Random Forest Algorithm ##########################################

scoring='balanced_accuracy'#balanced_accuracy'#, 'roc_auc', 'f1']

model_RF=RandomForestClassifier()
clf_RF=GridSearchCV(model_RF, grid_param_RF, scoring=scoring, cv=kfold, n_jobs=-1)
grid_result=clf_RF.fit(X_train, y_train)
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))


print('Best RF estimator',clf_RF.best_estimator_)
#print('The RF probability for the {} test samples that money will be recovered is:'.format(len(y_test)), np.squeeze(clf_RF.best_estimator_.predict_proba(X_test)[:,np.where(clf_RF.best_estimator_.classes_==1)])*100)
probability_predictions.append(['RF', np.squeeze(clf_RF.best_estimator_.predict_proba(X_test)[:,np.where(clf_RF.best_estimator_.classes_==1)])*100])
print("Balanced accuracy on the test set by RF:",balanced_accuracy_score(y_test,clf_RF.best_estimator_.predict(X_test)))


################################## Support Vector Machine Classifier ##################################


model_SVC=svm.SVC()
clf_SVC=GridSearchCV(model_SVC, grid_param_SVC, scoring=scoring, cv=kfold, n_jobs=-1)
grid_result=clf_SVC.fit(X_train, y_train)
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))


print('Best SVC estimator',clf_SVC.best_estimator_)
SVC_best=clf_SVC.best_estimator_.set_params(**{'probability':True})
SVC_best.fit(X_train, y_train)
#Probabilities with the SVM probabily only accessible when the data set is large enough since a Platt sclaing is applied to teh data set
#print(np.concatenate((SVC_best.predict_proba(X_test)*100, SVC_best.predict(X_test).reshape(-1,1)), axis=1)) #y_test.to_numpy().reshape(-1,1)), axis=1))
#print(SVC_best.predict(X_test))
#print(SVC_best.classes_)
#print('The SVC probability for the {} test samples that money will be recovered is:'.format(len(y_test)), np.squeeze(SVC_best.predict_proba(X_test)[:,np.where(SVC_best.classes_==1)])*100)
probability_predictions.append(['SVC', np.squeeze(SVC_best.predict_proba(X_test)[:,np.where(SVC_best.classes_==1)])*100])
print("Balanced accuracy on the test set by SVC:", balanced_accuracy_score(y_test, SVC_best.predict(X_test)))


#learning curve for SVC
param_range=range(0,100)
train_scores, test_scores=validation_curve(clf_SVC.best_estimator_, X_train, y_train, param_name='max_iter', param_range=param_range, cv=kfold,scoring='balanced_accuracy')

train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)

plt.title("Validation Curve with SVM")
plt.xlabel(r"iter")
plt.ylabel("balanced_accuracy")
plt.ylim(0.0, 1.1)
lw = 2
plt.plot(param_range, train_scores_mean, label="Training score",
             color="darkorange", lw=lw)
plt.fill_between(param_range, train_scores_mean - train_scores_std,
                 train_scores_mean + train_scores_std, alpha=0.2,
                 color="darkorange", lw=lw)
plt.plot(param_range, test_scores_mean, label="Cross-validation score",
             color="navy", lw=lw)
plt.fill_between(param_range, test_scores_mean - test_scores_std,
                 test_scores_mean + test_scores_std, alpha=0.2,
                 color="navy", lw=lw)
plt.legend(loc="lower right")
plt.savefig('Pictures/SVM_learning_curve_credit_scoring.pdf')
plt.close()


#####################################  Logistic Regression ####################################################

model_LR=LogisticRegression()
clf_LR=GridSearchCV(model_LR, grid_param_LR, scoring=scoring, cv=kfold, n_jobs=-1)
grid_result=clf_LR.fit(X_train, y_train)
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))


print('Best LR estimator',clf_LR.best_estimator_)
#print('The LR probability for the {} test samples that money will be recovered is:'.format(len(y_test)), np.squeeze(clf_LR.best_estimator_.predict_proba(X_test)[:,np.where(clf_LR.best_estimator_.classes_==1)])*100)
probability_predictions.append(['LR', np.squeeze(clf_LR.best_estimator_.predict_proba(X_test)[:,np.where(clf_LR.best_estimator_.classes_==1)])*100])
#print(clf_LR.best_estimator_.predict(X_test))
#print(cross_val_score(clf.best_estimator_, X_train, y_train, cv=2, scoring="balanced_accuracy"))
print("Balanced accuracy on the test set by LR:", balanced_accuracy_score(y_test, clf_LR.best_estimator_.predict(X_test)))

#################################### ANN algorithm (maybe a little bit clumsy programmed ###############################

class_weights = compute_class_weight('balanced',np.unique(y_train),y_train)

print(class_weights)
ANN=KerasClassifier(build_fn=create_model, verbose=0)
epochs = [2,3,4,5]
batches = [30,32,35,38,40]
param_grid=dict(epochs=epochs, batch_size=batches)
clf_ANN = GridSearchCV(estimator=ANN, param_grid=param_grid, cv=kfold, n_jobs=-1)
grid_result=clf_ANN.fit(X_train,y_train, class_weight=class_weights)
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))

ANN.set_params(**grid_result.best_params_)
history=ANN.fit(X_train,y_train, class_weight=class_weights)
probability_predictions.append(['ANN',np.squeeze(ANN.predict_proba(X_test)[:,np.where(ANN.classes_==1)]*100)])
print('Balanced accuracy on the test set by ANN:',balanced_accuracy_score(y_test, ANN.predict(X_test)))

#, ('ANN', clf_ANN.best_estimator_)

#################################### Another ensemble classifier with all primary choosen classifiers connected ##########

best_of_bests=VotingClassifier(estimators=[('SVC',SVC_best), ('RF',clf_RF.best_estimator_), ('LR',clf_LR.best_estimator_), ('ANN', clf_ANN.best_estimator_)], voting='soft')
best_of_bests.fit(X_train, y_train)
#print('The best-of-bests probability for the {} test samples that money will be recovered is:'.format(len(y_test)), np.squeeze(best_of_bests.predict_proba(X_test)[:,np.where(best_of_bests.classes_==1)])*100)
probability_predictions.append(['E_best', np.squeeze(best_of_bests.predict_proba(X_test)[:,np.where(best_of_bests.classes_==1)])*100])
print("Balanced accuracy on the test set of the Ensemble Best_of_Best:", balanced_accuracy_score(y_test,best_of_bests.predict(X_test)))

cross_val_scores=[]

for estimator in [dummy, clf_RF.best_estimator_, clf_LR.best_estimator_, SVC_best, best_eclf, best_of_bests, clf_ANN.best_estimator_]:
    score=cross_val_score(estimator, X_train, y_train, cv=kfold, scoring='balanced_accuracy')
    cross_val_scores.append([np.mean(score), np.std(score)])
cross_val_scores=np.asarray(cross_val_scores)
plt.title('Classifier Comparison')
plt.errorbar(np.arange(7), cross_val_scores[:,0], cross_val_scores[:,1], linestyle='None', marker='o', capsize=5, capthick=2)
plt.xticks(ticks=np.arange(7), labels=['Dummy','RF', 'LR', 'SVC', 'E_Hill', 'E_Bests', 'ANN'])
plt.xlabel("Classifier")
plt.ylabel('Balanced Accuracy')
plt.savefig('Pictures/credit_scoring_classifier_comparison.pdf')
#plt.show()
plt.close()
print(probability_predictions[:][1])
probability_predictions=pd.Series(np.array(probability_predictions)[:,1], index=np.array(probability_predictions)[:,0])

fig=probability_predictions.hist(by=probability_predictions.index.values,bins=20, figsize=(10,10),range=(0,100),layout=(3,3))
[[x.title.set_size(10), x.set_xlim(0,100), x.set_ylim(0,23), x.set_xlabel('prediction probability'), x.set_ylabel('prediction probability frequency')] for x in fig.ravel()]
plt.subplots_adjust(hspace=0.6, wspace=0.4)
plt.suptitle('Histogram of predicted probabilities for every classifier')
#plt.tight_layout()
plt.savefig('Pictures/credit_scoring_prediction_prob_hist')
plt.show()

#model=Sequential()
#model.add(Dense(200, activation='relu', input_dim=len(data.columns)))
#model.add(Dense(100, activation='relu'))
#model.add(Dense(50, activation='relu'))
#model.add(Dense(1, activation='sigmoid'))

#epochs=10
#model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
#hist=model.fit(X_train, y_train, epochs=epochs, batch_size=32, validation_data=(X_val,y_val))
#print(model.predict(X_test))
#_, accuracy=model.evaluate(X_test, y_test)

#print('Accuracy: %.2f' % (accuracy*100))

#plt.plot(hist.history['loss'])
#plt.plot(hist.history['val_loss'])
#plt.title('Model loss')
#plt.ylabel('Loss')
#plt.xlabel('Epoch')
#plt.legend(['Train', 'Val'], loc='upper right')
#plt.tight_layout()
#plt.savefig('Pictures/loss_func_ANN_credit_scoring_epochs_{}.pdf'.format(epochs))
#plt.close()