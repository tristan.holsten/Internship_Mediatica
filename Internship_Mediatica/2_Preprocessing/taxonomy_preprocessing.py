import pandas as pd
import numpy as np

#load datafile, data file has to be preprocessed a little bit by hand (see th Datatape_Emilbanca_taxonomy.csv) about how it has to look like
df= pd.read_csv("Data/Input/Datatape_Emilbanca_taxonomy.csv", sep=';', encoding='utf-8')

#fill every empty space in 'CLASSE' and 'SUB' column
for i in df.index:
    if  not pd.isnull(df.at[i, 'CLASSE']):
        substitute_Classe=df.at[i, 'CLASSE']
    else:
        df.at[i,'CLASSE']=substitute_Classe
    if  not pd.isnull(df.at[i, 'SUB']):
        substitute_Sub=df.at[i, 'SUB']
    else:
        df.at[i, 'SUB']=substitute_Sub

#assign multiindex for rows and columns (columns have form credit type with every ID as a subclass according to it's type)
df.set_index(['CLASSE','SUB','DOC SOTTOSTANTI'], inplace=True)
index_ID=df.columns.values
index_Tipo=df.loc[('Tipo', 'Tipo', 'Tipo')]
arrays = [index_Tipo, index_ID]
df.drop(index=('Tipo','Tipo', 'Tipo'), inplace=True)
df.columns = pd.MultiIndex.from_arrays(arrays, names=['Tipo','ID'])
#sort columns by the credit type
df.sort_index(level=0, axis=1, inplace=True)

#transpose dataframe (columns are now the document types and the rows the single cases
df_T=df.T
print(df_T.shape)

#extract target values (because these are not present inside the csv which is loaded in the next step)
target_df=df_T[['Andamento recupero', '% credito recuperato']]
df_T.drop(columns=['Andamento recupero', '% credito recuperato'], level=0, inplace=True)

#load taxonomy dataset
df_1= pd.read_csv("../1_Layout/Data/Output/taxonomy_preprocessed.csv", encoding='utf-8', index_col=['CLASSE','SUB','DOC SOTTOSTANTI'])
df_1.drop(columns='Sanitario', inplace=True)

#create to every credit type a own dataset by using the mask (taxonomy file)
for Type in df_1.columns.values:
    mask=df_1[Type].values
    features=df_T.loc[Type,mask]
    targets=target_df.loc[Type, :]
    export_type=pd.concat([features,targets], axis=1)
    print(export_type)
    print("ID'S in {} \n".format(Type), features.index)
    export_csv = export_type.to_csv(r'Data/Output/Datatape_Emilbanca_taxonomy_{}_preprocessed.csv'.format(Type), index=None, header=True)
