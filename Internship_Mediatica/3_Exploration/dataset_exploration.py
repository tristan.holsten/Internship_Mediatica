import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import copy
import seaborn as sns
from mpl_toolkits.axes_grid1 import make_axes_locatable

#load data file
df = pd.read_csv('../2_Preprocessing/Data/Output/Datatape_Emilbanca_taxonomy_Conti_preprocessed.csv', header=[0,1,2])

print(df.shape)
#extract target values (% credito recuperato), delete 'Andamento recupero', '% credito recuperato'
target_raw=df['% credito recuperato']
data=df.drop(columns=['Andamento recupero', '% credito recuperato'], level=0)

#print distribution of every single document
data_mod=data
data_mod.columns=data.columns.get_level_values(2)
print(data_mod.columns)
for i in range(11):
    fig=data_mod.hist(column=list(data_mod)[i*12:(i+1)*12],figsize=(15,10), layout=(3,4))
    [[x.title.set_size(10), x.set_xlabel('document presence'), x.set_ylabel('document frequency'),x.set_xticks([0,1]),x.set_xticklabels(['not present','present']), x.set_ylim(0,120), x.set_xlim([-0.2,1.2])] for x in fig.ravel()]
    plt.subplots_adjust(hspace=0.6, wspace=0.4)
    plt.savefig('Pictures/credit_scoring_data_exploration_distribution_{}.pdf'.format(i))
    plt.close()

#convert target array to binary targets
target_array=np.ones(len(target_raw.values))
#every credit was assumed to be closed and 0 was assigned to the credits where at least something of the given credit was recovered.
target_array[target_raw.values[:,0]>0]=0
target=pd.Series(target_array, name=('% credito recuperato', '% credito recuperato','% credito recuperato'))

#have a look at the distribution of the target values
print(sum(target)/len(target), (len(target)-sum(target))/len(target))

#delete every document which is always present or never present
unique_variables=[]
for (columnName, columnData) in data.iteritems():
    if len(np.unique(columnData.values))==1:
        unique_variables.append(columnName)
print('Number of documents deleted because of being always present or never:', len(unique_variables))
data.drop(columns=unique_variables, inplace=True)

#calculate the correlation between all documents and create a correlation matrix
feature_corr=data.corr()
plt.figure()
ax = plt.gca()
im=ax.matshow(feature_corr, vmin=-1, vmax=1)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
ax.set_xlabel('Document index')
ax.xaxis.set_label_position('top')
ax.set_ylabel('Document index')
plt.colorbar(im, cax=cax)
plt.savefig('Pictures/credit_scoring_data_exploration_heatmap_before.pdf')
plt.close()

#find all documents which have a correlation with an other document higher than 0.7
uncorr_features = np.full((feature_corr.shape[0],), True, dtype=bool)
for i in range(feature_corr.shape[0]):
   for j in range(i+1, feature_corr.shape[0]):
        if np.abs(feature_corr.iloc[i,j]) >= 0.7:
            if uncorr_features[j]:
                uncorr_features[j] = False
corr_features=np.invert(uncorr_features)
print('Number of documents deleted due to too high correlations:',sum(corr_features))

#calculate the correlation between all documents and create a correlation matrix after erasing all correlated documents
data=data[data.columns[uncorr_features]]
data_corr=data.corr()
plt.figure()
ax = plt.gca()
im=ax.matshow(data_corr, vmin=-1, vmax=1)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
ax.set_xlabel('Document index')
ax.xaxis.set_label_position('top')
ax.set_ylabel('Document index')
plt.colorbar(im, cax=cax)
plt.savefig('Pictures/credit_scoring_data_exploration_heatmap_after.pdf')
plt.close()
#plt.show()