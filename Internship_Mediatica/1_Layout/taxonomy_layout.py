import pandas as pd
import numpy as np

# import taxonomy file end replace X with True and 0 with False
# not very robust if spaces, tabs are inside the cells of the Tassonomia_Sintesi file
df= pd.read_csv("Data/Input/Tassonomia_SINTESI.csv", sep=';', encoding='utf-8', true_values=['X'],false_values=['0'] )

# fill all empty lines in superior rows with previous 'CLASSE' and 'SUBS'
for i in df.index:
    if  not pd.isnull(df.at[i, 'CLASSE']):
        substitute_Classe=df.at[i, 'CLASSE']
    else:
        df.at[i,'CLASSE']=substitute_Classe
    if  not pd.isnull(df.at[i, 'SUB']):
        substitute_Sub=df.at[i, 'SUB']
    else:
        df.at[i, 'SUB']=substitute_Sub

df.set_index(['CLASSE','SUB','DOC SOTTOSTANTI'], inplace=True)

#export taxonomy file with filled rows (no empty space should be present)
export_csv = df.to_csv (r'Data/Output/taxonomy_preprocessed.csv', index = True, header=True)
print("Taxonomy mask file created")